﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/22/2016
 * Time: 11:32 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Windows.Forms;

namespace SapUploadServices
{
	/// <summary>
	/// Description of ProductStorageDAL.
	/// </summary>
	public partial class ProductStorageDAL
	{
		public ProductStorageDAL()
		{
		}
		
		public static bool SaveUploadEsbResult(TsapReceiptUploadJobNo tsapReceiptUploadJobNo, List<TsapReceiptUploadModule> tsapReceiptUploadModules, string joinid, string posttime)
        {
            var listModulePacking = new List<MODULEPACKINGTRANSACTION>();
            var listCartonPacking = new List<CARTONPACKINGTRANSACTION>();

            #region 更新数据库
            using (var sqlConnection = new SqlConnection(SapUploadServices.LabelprintConn))
            {
                sqlConnection.Open();
                using (var sqlTransaction = sqlConnection.BeginTransaction())
                {
                    if (!InsertTsapReceiptUploadJobNo(tsapReceiptUploadJobNo, sqlConnection, sqlTransaction))
                    {
                        sqlTransaction.Rollback();
                        return false;
                    }
                    foreach (var tsapReceiptUploadModule in tsapReceiptUploadModules)
                    {
                        if (!UpdateTsapReceiptUploadModule(tsapReceiptUploadModule, sqlConnection, sqlTransaction))
                        {
                            sqlTransaction.Rollback();
                            return false;
                        }
                    }
                    try
                    {
                        var cartonNos = tsapReceiptUploadModules.Select(p => p.CartonNo).Distinct().ToList();
                        foreach (var cartonNo in cartonNos)
                        {
                            SaveStorageDataForReport(cartonNo, "Invertory", listModulePacking, listCartonPacking, joinid, posttime);
                            string msg;
                            if (!new CartonPackingTransactionDal().SaveStorage(sqlConnection, sqlTransaction, listCartonPacking, listModulePacking, out msg))
                            {
                                ToolsClass.WriteErrorLog("Record saving failed" + msg + "", "ABNORMAL","");
                                sqlTransaction.Rollback();
                                return false;
                            }
                        }

                        sqlTransaction.Commit();
                        return true;
                    }
                    catch
                    {
                        sqlTransaction.Rollback();
                        return false;
                    }
                }
            }
            #endregion
        }

		  public static DataSet ReadXmlToDataSet(string xmlString)
        {
            
        	StringReader sr = null;
            XmlTextReader xtr = null;
            DataSet ds = new DataSet();
            try
            {
                sr = new StringReader(xmlString);
                xtr = new XmlTextReader(sr);
                ds.ReadXml(xtr);
            }
            catch (Exception ex)
            {
                //Log
                throw ex;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (xtr != null)
                    xtr.Close();
            }

            return ds;
        }
		  
		  
		public static bool InsertTsapReceiptUploadJobNo(TsapReceiptUploadJobNo tsapReceiptUploadJobNo, SqlConnection conn, SqlTransaction trans)
        {
            const string sql = @"
			INSERT INTO TSAP_RECEIPT_UPLOAD_JOB_NO(SYSID,CREATED_ON,CREATED_BY,JOB_NO,INNER_JOB_NO,GROUP_HIST_KEY,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV10)
			VALUES(@Sysid,CONVERT(NVARCHAR(50),GETDATE(),121),@CreatedBy,@JobNo,@InnerJobNo,@GroupHistKey,@Resv01,@Resv02,@Resv03,@Resv04,@Resv05,@Resv06,@Resv07,@Resv08,@Resv09,@Resv10)
			
			";

            return Save(tsapReceiptUploadJobNo, sql, conn, trans);
        }  
		   public static bool UpdateTsapReceiptUploadModule(TsapReceiptUploadModule tsapReceiptUploadModule, SqlConnection conn, SqlTransaction trans)
        {
            const string sql = @"
			UPDATE TSAP_RECEIPT_UPLOAD_MODULE
			SET CREATED_BY=@CreatedBy,JOB_NO=@JobNo,CARTON_NO=@CartonNo,CUSTOMER_CARTON_NO=@CustomerCartonNo,INNER_JOB_NO=@InnerJobNo,FINISHED_ON=@FinishedOn,MODULE_COLOR=@ModuleColor,GROUP_HIST_KEY=@GroupHistKey,MODULE_SN=@ModuleSn,ORDER_NO=@OrderNo,SALES_ORDER_NO=@SalesOrderNo,SALES_ITEM_NO=@SalesItemNo,ORDER_STATUS=@OrderStatus,PRODUCT_CODE=@ProductCode,UNIT=@Unit,FACTORY=@Factory,WORKSHOP=@Workshop,PACKING_LOCATION=@PackingLocation,PACKING_MODE=@PackingMode,CELL_EFF=@CellEff,TEST_POWER=@TestPower,STD_POWER=@StdPower,MODULE_GRADE=@ModuleGrade,BY_IM=@ByIm,TOLERANCE=@Tolerance,CELL_CODE=@CellCode,CELL_BATCH=@CellBatch,CELL_PRINT_MODE=@CellPrintMode,GLASS_CODE=@GlassCode,GLASS_BATCH=@GlassBatch,EVA_CODE=@EvaCode,EVA_BATCH=@EvaBatch,TPT_CODE=@TptCode,TPT_BATCH=@TptBatch,CONBOX_CODE=@ConboxCode,CONBOX_BATCH=@ConboxBatch,CONBOX_TYPE=@ConboxType,LONG_FRAME_CODE=@LongFrameCode,LONG_FRAME_BATCH=@LongFrameBatch,SHORT_FRAME_CODE=@ShortFrameCode,SHORT_FRAME_BATCH=@ShortFrameBatch,GLASS_THICKNESS=@GlassThickness,IS_REWORK=@IsRework,IS_BY_PRODUCTION=@IsByProduction,UPLOAD_STATUS=@UploadStatus,RESV01=@Resv01,RESV02=@Resv02,RESV03=@Resv03,RESV04=@Resv04,RESV05=@Resv05,RESV06=@Resv06,RESV07=@Resv07,RESV08=@Resv08,RESV09=@Resv09,RESV10=@Resv10
			WHERE SYSID=@Sysid
			
			";

            return Save(tsapReceiptUploadModule, sql, conn, trans);
        }
        
		   internal static bool Save<T>(T t, string sql, SqlConnection conn, SqlTransaction trans)
        {
            var result = conn.Execute(sql, t, trans, 30, CommandType.Text);

            return result > 0;
        }
      
		   
		   
		   
		  public static void SaveStorageDataForReport(string carton, string Type, List<MODULEPACKINGTRANSACTION> _ListModulePacking,
          List<CARTONPACKINGTRANSACTION> _ListCartonPacking, string joinid, string posttime)
        {
            try
            {
                string sql = @"SELECT 
                             B.SN SN,
                             B.ModelType ModelType,
                             B.BOXID BOXID,
                             B.Cust_BoxID CUSTBOXID,
                             B.STDPOWER StdPower,
                             B.PMAX PMAX,
                             B.WorkOrder WORKORDER,
                             A.Temp TEMP,
                             A.VOC VOC,
                             A.ISC ISC,
                             A.Pmax PMAX,
                             A.Vm VM,
                             A.Im IM,
                             A.FF FF,
                             A.Eff EFF,
                             A.TestDateTime TESTTIME
                             FROM ElecParaTest A WITH(NOLOCK) INNER JOIN Product B WITH(NOLOCK) ON
                             A.FNumber =B.SN 
                             AND A.InterID=B.InterNewTempTableID
                              WHERE  B.BoxID  IN ('{0}')
                              ORDER BY B.BoxID,B.SN";
                sql = string.Format(sql, carton);
                DataTable dt = ToolsClass.PullData(sql,SapUploadServices.LabelprintConn);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (_ListCartonPacking.Count > 0)
                        _ListCartonPacking.Clear();

                    if (_ListModulePacking.Count > 0)
                        _ListModulePacking.Clear();
                    //string joinid = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");//交易ID
                    //string posttime = DT.DateTime().LongDateTime;

                    string cartonqty = "";
                    cartonqty = Convert.ToString(dt.Rows.Count.ToString());
                    #region
                    CARTONPACKINGTRANSACTION cartonpacking = new CARTONPACKINGTRANSACTION();
                    cartonpacking.SysID = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");
                    cartonpacking.Orgnization = "CS";
                    cartonpacking.Carton = carton;
                    cartonpacking.JobNo = "";
                    if (Type.Equals("Invertory"))
                    {
                        cartonpacking.Action = "Invertory";
                    }
                    else if (Type.Equals("UnInvertory"))
                    {
                        cartonpacking.Action = "UnInvertory";
                    }

                    cartonpacking.Resv01 = "CSAS";//2014-03-24
                    cartonpacking.Resv02 = Convert.ToString(dt.Rows[0]["CUSTBOXID"]);//2014-03-24
                    cartonpacking.ActionTxnID = joinid;
                    cartonpacking.ActionDate = posttime;
                    cartonpacking.ActionBy = "SYSTEMP";
                    cartonpacking.ModuleQty = cartonqty;
                    cartonpacking.StdPowerLevel = Convert.ToString(dt.Rows[0]["StdPower"]);
                    cartonpacking.CreatedBy = "Packing_M" + "CAMO".ToUpper().Trim().Substring("CAMO".ToUpper().Trim().Length - 2, 2);
                    _ListCartonPacking.Add(cartonpacking);
                    #endregion
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        #region
                        String modulesn = Convert.ToString(dt.Rows[i]["SN"]);
                        MODULEPACKINGTRANSACTION modulepacking = new MODULEPACKINGTRANSACTION();
                        modulepacking.SysID = Guid.NewGuid().ToString() + DateTime.Now.ToString("fff");
                        modulepacking.ModuleSn = modulesn;
                        modulepacking.CartonNoSysID = cartonpacking.SysID;
                        modulepacking.ProductNo = "";
                        modulepacking.ModuleType = Convert.ToString(dt.Rows[i]["ModelType"]);
                        modulepacking.ModuleStdPower = Convert.ToString(dt.Rows[i]["StdPower"]);
                        modulepacking.Pmax = Convert.ToString(dt.Rows[i]["Pmax"]);
                        modulepacking.ModuleStdPower = Convert.ToString(dt.Rows[i]["StdPower"]);
                        modulepacking.Workshop = "CAMO";
                        modulepacking.Temp = Convert.ToString(dt.Rows[i]["TEMP"]);
                        modulepacking.VOC = Convert.ToString(dt.Rows[i]["VOC"]);
                        modulepacking.ISC = Convert.ToString(dt.Rows[i]["ISC"]);
                        modulepacking.IMP = Convert.ToString(dt.Rows[i]["IM"]);
                        modulepacking.VMP = Convert.ToString(dt.Rows[i]["VM"]);
                        modulepacking.FF = Convert.ToString(dt.Rows[i]["FF"]);
                        modulepacking.EFF = Convert.ToString(dt.Rows[i]["EFF"]);
                        modulepacking.TestDate = Convert.ToString(dt.Rows[i]["TESTTIME"]);
                        modulepacking.WorkOrder = Convert.ToString(dt.Rows[i]["WORKORDER"]);
                        modulepacking.CreatedBy = "Packing_M" +  "CAMO".ToUpper().Trim().Substring("CAMO".ToUpper().Trim().Length - 2, 2);
                     _ListModulePacking.Add(modulepacking);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
            	ToolsClass.WriteErrorLog("Error","",ex.Message);
            	return;
            }
        }  
		
		   

		   
		  
		  
	}
}
