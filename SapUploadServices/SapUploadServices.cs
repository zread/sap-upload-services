﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/22/2016
 * Time: 4:30 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.ServiceProcess;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Xml;
using System.Threading;
using MySql.Data.MySqlClient;
using System.Data;

namespace SapUploadServices
{
	public class SapUploadServices : ServiceBase
	{
		public const string MyServiceName = "SapUploadServices";
		
		public static string CsiMesConn = ToolsClass.getConfig("conncsimes",false,"","Config.xml");
		public static string CenterConn = ToolsClass.getConfig("conncenter",false,"","Config.xml");
		public static string LabelprintConn = ToolsClass.getConfig("Conn",false,"","Config.xml");
		
		bool isrunning = false;
		private System.Timers.Timer timer1 = null;
		int Delay = Convert.ToInt32(ToolsClass.getConfig("Period",false,"","config.xml"));
		private Thread workerThread = null;
		
		
		public SapUploadServices()
		{
			InitializeComponent();
		}
		
		private void InitializeComponent()
		{
			this.ServiceName = MyServiceName;
		}
		private List<TsapReceiptUploadModule> _tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();
		
		
		protected override void OnStart(string[] args)
		{
			// TODO: Add start code here (if required) to start your service.
			ToolsClass.WriteToLog("Program running","");
			ToolsClass.SendEmail("Program started");
			workerThread = null;
			isrunning = true;
			timer1 = new System.Timers.Timer();
            this.timer1.Interval = 1000* (Delay);   
			this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            timer1.Enabled = true;			
		}
		
		
		protected override void OnStop()
		{			
			ToolsClass.WriteToLog("Program Stopped");
			ToolsClass.SendEmail("Program Stopped");
			if(workerThread!=null)
				workerThread.Abort();
			isrunning = false;	
		}
		
		
		
		void Up1loading()
		{
			string sql = @" select top 1 CARTON_NO from TSAP_RECEIPT_UPLOAD_MODULE with(nolock) where UPLOAD_STATUS = 'Ready' and CREATED_ON > '2016-08-01' and CREATED_ON <  dateadd(SECOND,-5,getdate()) and
							CARTON_NO not in (select CARTON_NO from [SapUploadStatus] with(nolock) where uploadstatus <> 0) order by CREATED_ON desc ";
			DataTable dt = ToolsClass.PullData(sql);
			if (dt == null || dt.Rows.Count < 1) {
				return;
			}
			
			if (_tsapReceiptUploadModules == null)
                _tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();

			var tsapReceiptUploadModules = FillEntries(dt.Rows[0][0].ToString(),
                    ToolsClass.getConfig ("Conn",false,"","Config.xml"));
			if (tsapReceiptUploadModules!=null) {
				_tsapReceiptUploadModules.AddRange(tsapReceiptUploadModules);
			}			
		}
		
		void Up2loading()
		{
			string CN = "";
			 try
            {
                if (_tsapReceiptUploadModules == null || _tsapReceiptUploadModules.Count <= 0)
                {
                	//ToolsClass.WriteToLog("Nothing exists","");
                    return;
                }
               
                var cartonNos = _tsapReceiptUploadModules.Select(p => p.CartonNo).Distinct().ToList();
                foreach (var cartonNo in cartonNos)
                {
                    string msg;
                    string sql = @" insert into [SapUploadStatus] values ('{0}',1,GETDATE(),'')";
                    sql = string.Format(sql,cartonNo);
                    ToolsClass.PutsData(sql);
                    CN = cartonNo;
                    if (IsHalfEsbSapCheck(cartonNo,out msg)) {
                    	ToolsClass.WriteToLog(cartonNo,msg);
                		return;
               		 }
                  
                }
                string jobNo = "NULL";
                
                string dateTime = DateTime.Now.ToShortDateString();
                string groupHistKey = "CAMO" + Guid.NewGuid().ToString("N");
                _tsapReceiptUploadModules.ForEach(p =>
                {
                    p.CreatedOn = dateTime;
                    p.CreatedBy = "SYSTEM";
                    p.JobNo = jobNo;
                    p.GroupHistKey = groupHistKey;
                    p.UploadStatus = "Finished";
                });
                
                 var tsapReceiptUploadJobNo = new TsapReceiptUploadJobNo
                {
                    Sysid = Guid.NewGuid().ToString("N"),
                    CreatedOn = dateTime,
                    CreatedBy = "SYSTEM",
                    JobNo = jobNo,
                    InnerJobNo = string.Empty,
                    GroupHistKey = groupHistKey,
                    Resv01 = "CAMO"
                };
                if (!UploadToEsb(_tsapReceiptUploadModules))
                	return;
              
                var actionTxnId = Guid.NewGuid().ToString("N") + DateTime.Now.ToString("fff");
                //var actionDate = DT.DateTime().LongDateTime; //jacky
                var actionDate = DateTime.Now.ToShortDateString();
//                string sqlupdate = @"update [SapUploadStatus] set [UploadStatus] = 1, msg = 'Uploading Succesful' where [Carton_no] = (  select top 1 [Carton_no]  FROM [SapUploadStatus] with(nolock)  order by ID desc)";
//                ToolsClass.PutsData(sqlupdate);
                
                if (!ProductStorageDAL.SaveUploadEsbResult(tsapReceiptUploadJobNo, _tsapReceiptUploadModules, actionTxnId, actionDate))
                {
                	ToolsClass.WriteErrorLog("Saving Upload record error","","");
                    return;
                }
                
                ToolsClass.WriteToLog("Upload Successful!");
                
            }
            catch (Exception ex)
            {
            	string sql =@"update [SapUploadStatus] set [UploadStatus] = 0 where [Carton_no] = '"+ CN+"'";
            	ToolsClass.PutsData(sql);
            	ToolsClass.WriteErrorLog("Failed to upload: ", ex.Message, "ABNORMAL");
            }
            finally
            {
            	ClearALL();
            }
		}
		
		private List<TsapReceiptUploadModule> FillEntries(string cartonNo, string connection)
		{
			 string sql = @"
		SELECT A.SYSID AS Sysid,A.CREATED_ON AS CreatedOn,A.CREATED_BY AS CreatedBy,A.JOB_NO AS JobNo,A.CARTON_NO AS CartonNo
		    ,A.CUSTOMER_CARTON_NO AS CustomerCartonNo,A.INNER_JOB_NO AS InnerJobNo,A.FINISHED_ON AS FinishedOn
		    ,A.MODULE_COLOR AS ModuleColor,A.GROUP_HIST_KEY AS GroupHistKey,A.MODULE_SN AS ModuleSn,A.ORDER_NO AS OrderNo
		    ,A.SALES_ORDER_NO AS SalesOrderNo,A.SALES_ITEM_NO AS SalesItemNo,A.ORDER_STATUS AS OrderStatus
		    ,A.PRODUCT_CODE AS ProductCode,A.UNIT AS Unit,A.FACTORY AS Factory,A.WORKSHOP AS Workshop
		    ,A.PACKING_LOCATION AS PackingLocation,A.PACKING_MODE AS PackingMode,A.CELL_EFF AS CellEff,A.TEST_POWER AS TestPower
		    ,A.STD_POWER AS StdPower,A.MODULE_GRADE AS ModuleGrade,A.BY_IM AS ByIm,A.TOLERANCE AS Tolerance,A.CELL_CODE AS CellCode
		    ,A.CELL_BATCH AS CellBatch,A.CELL_PRINT_MODE AS CellPrintMode,A.GLASS_CODE AS GlassCode,A.GLASS_BATCH AS GlassBatch
		    ,A.EVA_CODE AS EvaCode,A.EVA_BATCH AS EvaBatch,A.TPT_CODE AS TptCode,A.TPT_BATCH AS TptBatch,A.CONBOX_CODE AS ConboxCode
		    ,A.CONBOX_BATCH AS ConboxBatch,A.CONBOX_TYPE AS ConboxType,A.LONG_FRAME_CODE AS LongFrameCode
		    ,A.LONG_FRAME_BATCH AS LongFrameBatch,A.SHORT_FRAME_CODE AS ShortFrameCode,A.SHORT_FRAME_BATCH AS ShortFrameBatch
		    ,A.GLASS_THICKNESS AS GlassThickness,A.IS_REWORK AS IsRework,A.IS_BY_PRODUCTION AS IsByProduction
		    ,A.UPLOAD_STATUS AS UploadStatus,A.RESV01 AS Resv01,A.RESV02 AS Resv02,A.RESV03 AS Resv03,A.RESV04 AS Resv04
		    ,A.RESV05 AS Resv05,A.RESV06 AS Resv06,A.RESV07 AS Resv07,A.RESV08 AS Resv08,A.RESV09 AS Resv09,A.RESV10 AS Resv10
		FROM TSAP_RECEIPT_UPLOAD_MODULE AS A WITH(NOLOCK) 
		WHERE 1 = 1 
		    AND A.CARTON_NO = '{0}'AND A.UPLOAD_STATUS = 'Ready' ";
			 sql = string.Format(sql,cartonNo);
			
			   		
			 
			 DataTable dt = ToolsClass.PullData(sql,connection);
			 if (dt == null || dt.Rows.Count < 0 ) {
			 	return null;
			 }
			 //dataGridView1.DataSource = dt;
//			 List<TsapReceiptUploadModule> list = dt.AsEnumerable()
//                           .Select(r=> r.Field<TsapReceiptUploadModule>("CartonNo"))
//                           .ToList();
//			 
			 List<TsapReceiptUploadModule> list = (from DataRow row in dt.Rows
			   select new TsapReceiptUploadModule
			   {
			   		CartonNo = cartonNo,
			   		Sysid = row["Sysid"].ToString(),
			   		CreatedOn = row["CreatedOn"].ToString(),
			   		CreatedBy = row["CreatedBy"].ToString(),
			   		JobNo = row["JobNo"].ToString(),
			   		CustomerCartonNo = row["CustomerCartonNo"].ToString(),
			   		InnerJobNo = row["InnerJobNo"].ToString(),
			   		FinishedOn = row["FinishedOn"].ToString(),
			   		ModuleColor = row["ModuleColor"].ToString(),
			   		GroupHistKey = row["GroupHistKey"].ToString(),
			   		ModuleSn = row["ModuleSn"].ToString(),
			   		OrderNo = row["OrderNo"].ToString(),
			   		SalesOrderNo = row["SalesOrderNo"].ToString(),
			   		SalesItemNo = row["SalesItemNo"].ToString(),
			   		OrderStatus = row["OrderStatus"].ToString(),
			   		ProductCode = row["ProductCode"].ToString(),
			   		Unit = row["Unit"].ToString(),
			   		Factory = row["Factory"].ToString(),
			   		Workshop = row["Workshop"].ToString(),
			   		PackingLocation = row["PackingLocation"].ToString(),
			   		PackingMode = row["PackingMode"].ToString(),
			   		CellEff = row["CellEff"].ToString(),
			   		TestPower = row["TestPower"].ToString(),
			   		StdPower = row["StdPower"].ToString(),
			   		ModuleGrade = row["ModuleGrade"].ToString(),
			   		ByIm = row["ByIm"].ToString(),
			   		Tolerance = row["Tolerance"].ToString(),
			   		CellCode = row["CellCode"].ToString(),
			   		CellBatch = row["CellBatch"].ToString(),
			   		CellPrintMode = row["CellPrintMode"].ToString(),
			   		GlassCode = row["GlassCode"].ToString(),
			   		GlassBatch = row["GlassBatch"].ToString(),
			   		EvaCode = row["EvaCode"].ToString(),
			   		EvaBatch = row["EvaBatch"].ToString(),
			   		TptCode = row["TptCode"].ToString(),
			   		TptBatch = row["TptBatch"].ToString(),
					ConboxCode= row["ConboxCode"].ToString(),
			   		ConboxBatch= row["ConboxBatch"].ToString(),
			   		ConboxType= row["ConboxType"].ToString(),
			   		LongFrameCode= row["LongFrameCode"].ToString(),
			   		LongFrameBatch= row["LongFrameBatch"].ToString(),
			   		ShortFrameCode= row["ShortFrameCode"].ToString(),
			   		ShortFrameBatch= row["ShortFrameBatch"].ToString(),
			   		GlassThickness= row["GlassThickness"].ToString(),
			   		IsRework= row["IsRework"].ToString(),
			   		IsByProduction= row["IsByProduction"].ToString(),
			   		UploadStatus = row["UploadStatus"].ToString(),
			   		Resv01 = row["Resv01"].ToString(),
			   		Resv02 = row["Resv02"].ToString(),
			   		Resv03 = row["Resv03"].ToString(),
			   		Resv04 = row["Resv04"].ToString(),
			   		Resv05 = row["Resv05"].ToString(),
			   		Resv06 = row["Resv06"].ToString(),
			   		Resv07 = row["Resv07"].ToString(),
			   		Resv08 = row["Resv08"].ToString(),
			   		Resv09 = row["Resv09"].ToString(),
			   		Resv10 = row["Resv10"].ToString()
			   			
			   }).ToList(); 
			 
			 
			 return list;
			 
			  
	
        }
		 private bool IsHalfEsbSapCheck(string Carton,out string message)
        {
        	         
        	
        	string sql = @"select MAPPING_KEY_02,MAPPING_KEY_04,MAPPING_KEY_05,MAPPING_KEY_06
						  	FROM [csimes_SapInterface].[dbo].[T_SYS_MAPPING] with(nolock) where FUNCTION_CODE = 'PackingSystemEsbInterfaceAddress' 
						    AND MAPPING_KEY_01 = 'CAMO'";
        	DataTable dt = ToolsClass.PullData(sql,CsiMesConn);
        	
        	if (dt == null || dt.Rows.Count < 1) {
        		message = "Carton" + Carton + " query ESB connection error";
        		return true;
        	}
        		
        	string host = dt.Rows[0][0].ToString().Substring(7,12);
        	string user = dt.Rows[0][1].ToString();
        	string password = dt.Rows[0][2].ToString();
        	string dbcon = dt.Rows[0][3].ToString();
        	bool ret = false;
        	sql = "";
					#region leave the carton at 2nd call
						sql = " Select * from stockin where Carton_no = '{0}' and  process_flag = '3' and status = 'F'";
						sql = string.Format(sql,Carton);
						MySqlConnection con1 = new MySqlConnection("host="+ host +";user= "+ user +";password="+password+";database="+dbcon+";");					
						MySqlCommand cmd1 = new MySqlCommand(sql, con1);
						con1.Open();
						cmd1 = new MySqlCommand(sql, con1);
						
					try 
					{
						MySqlDataReader  reader  = cmd1.ExecuteReader();
						if(reader!= null && reader.Read())
						{
							message = "Carton No.: "+Carton+ " is waiting for pack in SAP, re-submit not required!";
							ToolsClass.SendEmail(message);
							ret = true;							
						}
						message = "";
						reader.Close();
						return ret;
					} 
					catch (MySql.Data.MySqlClient.MySqlException ex) {
						message = ex.Message;
        				return true;
						
					}
					finally
					{
						con1.Close();
						con1.Dispose();
					}
						
					#endregion
					               
        
		}       
	 
		 private bool UploadToEsb(List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            //var url = DataAccess.QueryEsbInterfaceAddress(FormCover.CurrentFactory, FormCover.InterfaceConnString) +"SAPInventory"; 
           	
            string sql = @"select MAPPING_KEY_02
						  	FROM [csimes_SapInterface].[dbo].[T_SYS_MAPPING] with(nolock)  where FUNCTION_CODE = 'PackingSystemEsbInterfaceAddress' 
						    AND MAPPING_KEY_01 = 'CAMO'";
           DataTable dt = ToolsClass.PullData(sql,CsiMesConn);
            if (dt == null || dt.Rows.Count < 1) {
           		return false;
            }
            
           var url = dt.Rows[0][0].ToString() +"CanadianSolar/QuerySAPInventoryStatus";
			

            //var req =WebRequest.Create(url);
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url); 
           
    
            req.KeepAlive = false;
            req.Timeout = 120000;
            req.ServicePoint.ConnectionLeaseTimeout = 120000;
    		req.ServicePoint.MaxIdleTime = 120000;
            
			
            ToolsClass.WriteToLog("UploadToEsb::" + url,"");
            
            req.Method = "POST";
            var soap = BuildSoapXml(tsapReceiptUploadModules);

            req.ContentType = "text/xml;charset=UTF-8";

           
            //ToolsClass.WriteToLog("UploadToEsb::" + soap,"");
            //字符转字节
            var bytes = Encoding.UTF8.GetBytes(soap);           

            var writer = req.GetRequestStream();
            writer.Write(bytes, 0, bytes.Length);
            writer.Flush();
            writer.Close();
            try
            {
                //返回 HttpWebResponse
               // var hwRes = req.GetResponse() as HttpWebResponse;
                HttpWebResponse hwRes = req.GetResponse() as HttpWebResponse;
                string result;
                if (hwRes == null)
                {
                    ToolsClass.WriteToLog("ABNORMAL", "Retrun null");

                    return false;
                }
                if (hwRes.StatusCode == HttpStatusCode.OK)
                {
                    //是否返回成功
                    var rStream = hwRes.GetResponseStream();
                    //流读取
                    var sr = new StreamReader(rStream, Encoding.UTF8);
                    result = sr.ReadToEnd();
                    sr.Close();
                    rStream.Close();
                }
                else
                {
                    result = "connection error ";
                    sql = @"update [SapUploadStatus] set [UploadStatus] = 0, msg = 'connection error' where [Carton_no] = (  select top 1 [Carton_no] FROM [SapUploadStatus] with(nolock)  order by ID desc)";
                    ToolsClass.PutsData(sql);
                }
                //关闭
                hwRes.Close();
                result = result.ToUpper();
                ToolsClass.WriteToLog("UploadToEsb::" + result,"");
                result = result.Replace(@"<?XML VERSION=""1.0"" ENCODING=""UTF-8""?>", string.Empty);
                result = result.Replace(@"<SOAP:ENVELOPE XMLNS:SOAP=""HTTP://SCHEMAS.XMLSOAP.ORG/SOAP/ENVELOPE/"">", string.Empty);
                result = result.Replace(@"<ENTERWAREHOUSERESPONSE XMLNS=""HTTP://ESB.CANADIANSOLAR.COM"">",@"<ENTERWAREHOUSERESPONSE>");
                result = result.Replace(@"<SOAP:BODY>", string.Empty);
                result = result.Replace(@"</SOAP:BODY>", string.Empty);
                result = result.Replace(@"</SOAP:ENVELOPE>", string.Empty);
                //yuhua
                result = result.Replace(@"<RETURN>", string.Empty);
				result = result.Replace(@"</RETURN>", string.Empty);

                #region testing
//		                using (System.IO.StreamWriter file = 
//		            new System.IO.StreamWriter(@"C:\Users\Jacky.li\Documents\Jacky\WriteLineschina.txt"))
//		        {
//		           
//                	file.WriteLine(result.ToString());
//		                
//		            
//		        }
                #endregion
                
                
                var ds = ProductStorageDAL.ReadXmlToDataSet(result);
                if (ds == null || ds.Tables == null || ds.Tables.Count <= 0 ||
                    ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count <= 0)
                {
                    ToolsClass.WriteToLog("Connect", "ABNORMAL");
                    return false;
                }
                var row = ds.Tables[0].Rows[0];
                var isSuccess = row[0].ToString().ToUpper();
                var message = row[1].ToString();
                if (isSuccess != "true".ToUpper())
                {
					//By pass carton exist error in SAP.Jacky 2016.05.10
                	if(message.Substring(message.IndexOf("在SAP")+4,6).Equals("系统中已存在"))
                	{
						sql =@"update [SapUploadStatus] set [UploadStatus] = 1, msg = 'Exist IN SAP' where [Carton_no] = (  select top 1 [Carton_no]  FROM [SapUploadStatus]   order by ID desc)";
            			ToolsClass.PutsData(sql);
            			return true;
                	}
                	else if(message.IndexOf("电池片晶面不能为空")!=-1)
                	{
                		ToolsClass.WriteToLog("Cell pattern cannot be empty, Materialcode/Batch no. "+message.Substring(message.IndexOf("电池片晶面不能为空")+17,29),"ABNORMAL");
                		sql =@"update [SapUploadStatus] set [UploadStatus] = 2, msg = 'Cell pattern cannot be empty, Materialcode/Batch no.' where [Carton_no] = (  select top 1 [Carton_no] FROM [SapUploadStatus] with(nolock)  order by ID desc)";
            			ToolsClass.PutsData(sql);	
                	}
                	else if(message.IndexOf("PROCESSED BY")!=-1 ||message.IndexOf("LOCKED BY")!=-1 || message.IndexOf("SYSTEM IS BUSY – PLEASE TRY AGAIN IN A LITTLE WHILE")!=-1)
                	{
                		ToolsClass.WriteToLog(message,"ABNORMAL");
                		sql =@"update [SapUploadStatus] set [UploadStatus] = 0, msg = 'Sap Transection locked will try later' where [Carton_no] = (  select top 1 [Carton_no]  FROM [SapUploadStatus] with(nolock)  order by ID desc)";
            			ToolsClass.PutsData(sql);
                	}
                	else
                	{
                		ToolsClass.WriteToLog("upload failed", message);
                		sql =@"update [SapUploadStatus] set [UploadStatus] = 2, msg = '"+message.Substring(100,400)+"' where [Carton_no] = (  select top 1 [Carton_no]  FROM [SapUploadStatus] with(nolock)  order by ID desc)";
                		ToolsClass.PutsData(sql);	
                	}
                	
                	string sqlcheck = @"select Carton_no from SapUploadStatus where [Carton_no] = ( select top 1 [Carton_no]  FROM [SapUploadStatus] with(nolock)  order by ID desc) and UploadStatus <> 1 ";
                	DataTable dtcheck = ToolsClass.PullData(sqlcheck);
                	if (dtcheck != null && dtcheck.Rows.Count == 1) 
                	{
                		ToolsClass.SendEmail("Carton: "+ dtcheck.Rows[0][0].ToString() + message);
                	}
                	ToolsClass.WriteErrorLog(message,"","");
                    return false;
                }
                sql =@"update [SapUploadStatus] set [UploadStatus] = 1, msg = 'Uploading Succesful' where [Carton_no] = (  select top 1 [Carton_no]  FROM [SapUploadStatus] with(nolock)  order by ID desc)";
                ToolsClass.PutsData(sql);	
                return true;
            }
            catch (WebException ex)
            {
                var responseFromServer = ex.Message + " ";
                if (ex.Response != null)
                {
                    using (var response = ex.Response)
                    {
                        var data = response.GetResponseStream();
                        using (var reader = new StreamReader(data))
                        {
                            responseFromServer += reader.ReadToEnd();
                        }
                    }
                }
                ToolsClass.WriteErrorLog(responseFromServer, "ABNORMAL", "");
                return false;
            }
            catch (Exception ex)
            {
                ToolsClass.WriteErrorLog(ex.Message, "ABNORMAL", "");
                return false;
            }
            finally
            {
            	req.Abort();
            }
        }	 
			 
		 private void ClearALL()
		 {
            _tsapReceiptUploadModules = new List<TsapReceiptUploadModule>();            
         }	 
		
		
		 private string BuildSoapXml(List<TsapReceiptUploadModule> tsapReceiptUploadModules)
        {
            var cartons =
                tsapReceiptUploadModules.Select(
                    p => new { p.CartonNo, p.CustomerCartonNo, p.InnerJobNo, p.JobNo, p.ModuleColor }).
                    Distinct().ToList();
			
            
            
           // Encoding Utf8 = new UTF8Encoding(false);     //Initializes a new instance of the UTF8Encoding class. A parameter specifies whether to provide a Unicode byte order mark.  
		         
            
            var st = new MemoryStream();
            //初始化一个XmlTextWriter,编码方式为Encoding.UTF8
            
            var tr = new XmlTextWriter(st, Encoding.UTF8);
            tr.WriteStartDocument();
			
            tr.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");//soap:Envelope
           //tr.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/wsdl");//soap:Envelope
            tr.WriteAttributeString("xmlns", "soap", null, "http://schemas.xmlsoap.org/soap/envelope/");

            tr.WriteStartElement("Header", "http://schemas.xmlsoap.org/soap/envelope/");//soap:Header
            tr.WriteStartElement(null, "RequestHeader", "http://esb.canadiansolar.com");//RequestHeader ***changed this from NULL
            tr.WriteElementString("AppCode", "PackingSystem_" + "CAMO");
            tr.WriteElementString("Version", "1");
            tr.WriteElementString("ClientTimestamp", tsapReceiptUploadModules[0].CreatedOn);
            tr.WriteElementString("GUID", tsapReceiptUploadModules[0].GroupHistKey);
            tr.WriteEndElement();//RequestHeader
            tr.WriteEndElement();//soap:Header

            tr.WriteStartElement("Body", "http://schemas.xmlsoap.org/soap/envelope/");//soap:Body
            tr.WriteStartElement(null, "EnterWarehouse", "http://esb.canadiansolar.com");//EnterWarehouse ***changed this from NULL
            tr.WriteStartElement(null, "PackingData", null);//PackingData
            //循环Carton
            for (var i = 0; i < cartons.Count; i++)
            {
                var carton = cartons[i];
                var findByCartonNo =
                    tsapReceiptUploadModules.FindAll(p => p.CartonNo == carton.CartonNo)
                        .OrderBy(p => p.FinishedOn)
                        .Take(1).FirstOrDefault();
                tr.WriteStartElement(null, "Carton", null); //Carton
                tr.WriteElementString("CartonNo", carton.CartonNo);
                ToolsClass.WriteToLog("Writing: ",carton.CartonNo);
                tr.WriteElementString("CustomerCartonNo", carton.CustomerCartonNo);
                tr.WriteElementString("InnerJobNo", carton.InnerJobNo);
                tr.WriteElementString("JobNo", carton.JobNo);
                //tr.WriteElementString("FinishedOn", findByCartonNo.FinishedOn);
                tr.WriteElementString("FinishedOn", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));//jacky
                //tr.WriteElementString("FinishedOn", DTpick.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                
                tr.WriteElementString("ModuleColor", carton.ModuleColor);
                tr.WriteStartElement(null, "Modules", null); //Modules
                //循环Module
                var modules = tsapReceiptUploadModules.FindAll(p => p.CartonNo == carton.CartonNo);
                foreach (var module in modules)
                {
                    tr.WriteStartElement(null, "Module", null); //Module
                    tr.WriteElementString("ModuleSN", module.ModuleSn);
                    tr.WriteElementString("OrderNo", module.OrderNo);
                    tr.WriteElementString("SalesOrderNo", module.SalesOrderNo);
                    tr.WriteElementString("SalesItemNo", module.SalesItemNo);
                    tr.WriteElementString("OrderStatus", module.OrderStatus);
                    tr.WriteElementString("ProductCode", module.ProductCode);
                    tr.WriteElementString("Unit", module.Unit);
                    tr.WriteElementString("Factory", module.Factory);
                    tr.WriteElementString("Workshop", module.Workshop);
                    tr.WriteElementString("PackingLocation", module.PackingLocation);
                    tr.WriteElementString("PackingMode", module.PackingMode);
                    tr.WriteElementString("CellEff", module.CellEff);
                    tr.WriteElementString("TestPower", module.TestPower);
                    tr.WriteElementString("StdPower", module.StdPower);
                    tr.WriteElementString("ModuleGrade", module.ModuleGrade);
                    tr.WriteElementString("ByIm", module.ByIm);
                    tr.WriteElementString("Tolerance", module.Tolerance);
                    tr.WriteElementString("CellCode", module.CellCode);
                    tr.WriteElementString("CellBatch", module.CellBatch);
                    tr.WriteElementString("CellPrintMode", module.CellPrintMode);
                    tr.WriteElementString("GlassCode", module.GlassCode);
                    tr.WriteElementString("GlassBatch", module.GlassBatch);
                    tr.WriteElementString("EvaCode", module.EvaCode);
                    tr.WriteElementString("EvaBatch", module.EvaBatch);
                    tr.WriteElementString("TptCode", module.TptCode);
                    tr.WriteElementString("TptBatch", module.TptBatch);
                    tr.WriteElementString("ConBoxCode", module.ConboxCode);
                    tr.WriteElementString("ConBoxBatch", module.ConboxBatch);
                    tr.WriteElementString("ConBoxType", module.ConboxType);
                    tr.WriteElementString("LongFrameCode", module.LongFrameCode);
                    tr.WriteElementString("LongFrameBatch", module.LongFrameBatch);
                    tr.WriteElementString("ShortFrameCode", module.ShortFrameCode);
                    tr.WriteElementString("ShortFrameBatch", module.ShortFrameBatch);
                    tr.WriteElementString("GlassThickness", module.GlassThickness);
                    tr.WriteElementString("IsRework", module.IsRework);
                    tr.WriteElementString("IsByProduction", module.IsByProduction);
                    tr.WriteElementString("StdPowerLevel", module.Resv01);
                    tr.WriteElementString("Market", module.Resv02);
                    tr.WriteElementString("LID", module.Resv03); //jakcy20160203

                    tr.WriteEndElement(); //Module
                }
                tr.WriteEndElement(); //Modules
                tr.WriteEndElement(); //Carton 
            }

            tr.WriteEndElement();//PackingData
            tr.WriteEndElement();//EnterWarehouse
            tr.WriteEndElement();//soap:Body

            tr.WriteEndElement();//soap:Envelope

            tr.WriteEndDocument();

            tr.Flush();

            var bytes = st.ToArray();
            st.Close();
            return Encoding.UTF8.GetString(bytes);
        }
		 
		  void timer1_Tick(object sender, EventArgs e)
        {
        	if(!isrunning)
        	{
        		timer1.Stop();
        		return;
        	}
        	if(workerThread != null)
        	{
        		//ToolsClass.WriteToLog(workerThread.ThreadState.ToString());
        		if(workerThread.ThreadState.ToString() == "Running")
        		{
        			ToolsClass.WriteToLog("Program still Running");
        			return;
        		}
        		workerThread.Abort();    		
        		
        	}
        	
        	this.workerThread = new Thread(new ThreadStart(Uploading));
		    this.workerThread.Start();
        }
        
		 void Uploading()
		 {
		 	string sql = "select AutoUpload from [TECO] where AutoUpload = 'True' ";
		 	DataTable dt = ToolsClass.PullData(sql);
		 	if (dt == null || dt.Rows.Count < 1) 
		 		return;	
		 	
		 	Up1loading();
		 	Up2loading();
		 }
		
		
		
		
		
	}
}
