﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/22/2016
 * Time: 9:06 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace SapUploadServices
{
	/// <summary>
	/// Description of DapperEntities.
	/// </summary>
	public partial class DapperEntities 
	{
		public DapperEntities()
		{
			
		}
	}
	
	   #region TsapReceiptUploadModule
    //
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TsapReceiptUploadModule
    {
        /// <summary>
        /// 入库手动上传数据主键
        /// </summary>
        public String Sysid { get; set; }
        /// <summary>
        /// 上传时间
        /// </summary>
        public String CreatedOn { get; set; }
        /// <summary>
        /// 上传操作工
        /// </summary>
        public String CreatedBy { get; set; }
        /// <summary>
        /// 出货柜号
        /// </summary>
        public String JobNo { get; set; }
        /// <summary>
        /// 托号
        /// </summary>
        public String CartonNo { get; set; }
        /// <summary>
        /// 客户自定义托号
        /// </summary>
        public String CustomerCartonNo { get; set; }
        /// <summary>
        /// 自定义柜号
        /// </summary>
        public String InnerJobNo { get; set; }
        /// <summary>
        /// 打托时间
        /// </summary>
        public String FinishedOn { get; set; }
        /// <summary>
        /// 组件颜色
        /// </summary>
        public String ModuleColor { get; set; }
        /// <summary>
        /// 标记同一次上传的数据的Key
        /// </summary>
        public String GroupHistKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ModuleSn { get; set; }
        /// <summary>
        /// 生产订单号
        /// </summary>
        public String OrderNo { get; set; }
        /// <summary>
        /// 销售订单
        /// </summary>
        public String SalesOrderNo { get; set; }
        /// <summary>
        /// 销售订单行项目
        /// </summary>
        public String SalesItemNo { get; set; }
        /// <summary>
        /// 生产订单状态
        /// </summary>
        public String OrderStatus { get; set; }
        /// <summary>
        /// 产品物料代码
        /// </summary>
        public String ProductCode { get; set; }
        /// <summary>
        /// 单位 固定值：PC
        /// </summary>
        public String Unit { get; set; }
        /// <summary>
        /// 工厂
        /// </summary>
        public String Factory { get; set; }
        /// <summary>
        /// 车间
        /// </summary>
        public String Workshop { get; set; }
        /// <summary>
        /// 入库地点
        /// </summary>
        public String PackingLocation { get; set; }
        /// <summary>
        /// 包装方式：A横包装 B竖包装 C双件包 D单件包 E其它
        /// </summary>
        public String PackingMode { get; set; }
        /// <summary>
        /// 电池片转换效率
        /// </summary>
        public String CellEff { get; set; }
        /// <summary>
        /// 实测功率
        /// </summary>
        public String TestPower { get; set; }
        /// <summary>
        /// 标称功率
        /// </summary>
        public String StdPower { get; set; }
        /// <summary>
        /// 组件等级
        /// </summary>
        public String ModuleGrade { get; set; }
        /// <summary>
        /// 电流分档 传入实测电流值，SAP接收之后，如果有值，认为是 Y 表示是否做了电池分档
        /// </summary>
        public String ByIm { get; set; }
        /// <summary>
        /// 公差
        /// </summary>
        public String Tolerance { get; set; }
        /// <summary>
        /// 电池片物料号
        /// </summary>
        public String CellCode { get; set; }
        /// <summary>
        /// 电池片批次
        /// </summary>
        public String CellBatch { get; set; }
        /// <summary>
        /// 电池片网版本
        /// </summary>
        public String CellPrintMode { get; set; }
        /// <summary>
        /// 玻璃物料号
        /// </summary>
        public String GlassCode { get; set; }
        /// <summary>
        /// 玻璃批次
        /// </summary>
        public String GlassBatch { get; set; }
        /// <summary>
        /// EVA物料号
        /// </summary>
        public String EvaCode { get; set; }
        /// <summary>
        /// EVA批次
        /// </summary>
        public String EvaBatch { get; set; }
        /// <summary>
        /// 背板物料号
        /// </summary>
        public String TptCode { get; set; }
        /// <summary>
        /// 背板批次
        /// </summary>
        public String TptBatch { get; set; }
        /// <summary>
        /// 接线盒物料号
        /// </summary>
        public String ConboxCode { get; set; }
        /// <summary>
        /// 接线盒批次
        /// </summary>
        public String ConboxBatch { get; set; }
        /// <summary>
        /// 接线盒类型 目前为空，该节点可以不传
        /// </summary>
        public String ConboxType { get; set; }
        /// <summary>
        /// 长边框物料号
        /// </summary>
        public String LongFrameCode { get; set; }
        /// <summary>
        /// 长边框批次
        /// </summary>
        public String LongFrameBatch { get; set; }
        /// <summary>
        /// 短边框物料号
        /// </summary>
        public String ShortFrameCode { get; set; }
        /// <summary>
        /// 短边框批次
        /// </summary>
        public String ShortFrameBatch { get; set; }
        /// <summary>
        /// 玻璃厚度：3.2mm 4mm Other
        /// </summary>
        public String GlassThickness { get; set; }
        /// <summary>
        /// 是否重工，Y表示重工，N表示不重工
        /// </summary>
        public String IsRework { get; set; }
        /// <summary>
        /// 是否副产品
        /// </summary>
        public String IsByProduction { get; set; }
        /// <summary>
        /// 上传状态：包装系统使用 Ready/Finished/Canceled
        /// </summary>
        public String UploadStatus { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv01 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv02 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv03 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv04 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv05 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv06 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv07 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv08 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv09 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv10 { get; set; }

        public string CommandName { get; set; }

        //构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public TsapReceiptUploadModule()
        {
            Sysid = string.Empty;
            CreatedOn = string.Empty;
            CreatedBy = string.Empty;
            JobNo = string.Empty;
            CartonNo = string.Empty;
            CustomerCartonNo = string.Empty;
            InnerJobNo = string.Empty;
            FinishedOn = string.Empty;
            ModuleColor = string.Empty;
            GroupHistKey = string.Empty;
            ModuleSn = string.Empty;
            OrderNo = string.Empty;
            SalesOrderNo = string.Empty;
            SalesItemNo = string.Empty;
            OrderStatus = string.Empty;
            ProductCode = string.Empty;
            Unit = string.Empty;
            Factory = string.Empty;
            Workshop = string.Empty;
            PackingLocation = string.Empty;
            PackingMode = string.Empty;
            CellEff = string.Empty;
            TestPower = string.Empty;
            StdPower = string.Empty;
            ModuleGrade = string.Empty;
            ByIm = string.Empty;
            Tolerance = string.Empty;
            CellCode = string.Empty;
            CellBatch = string.Empty;
            CellPrintMode = string.Empty;
            GlassCode = string.Empty;
            GlassBatch = string.Empty;
            EvaCode = string.Empty;
            EvaBatch = string.Empty;
            TptCode = string.Empty;
            TptBatch = string.Empty;
            ConboxCode = string.Empty;
            ConboxBatch = string.Empty;
            ConboxType = string.Empty;
            LongFrameCode = string.Empty;
            LongFrameBatch = string.Empty;
            ShortFrameCode = string.Empty;
            ShortFrameBatch = string.Empty;
            GlassThickness = string.Empty;
            IsRework = string.Empty;
            IsByProduction = string.Empty;
            UploadStatus = "Ready";
            Resv01 = string.Empty;
            Resv02 = string.Empty;
            Resv03 = string.Empty;
            Resv04 = string.Empty;
            Resv05 = string.Empty;
            Resv06 = string.Empty;
            Resv07 = string.Empty;
            Resv08 = string.Empty;
            Resv09 = string.Empty;
            Resv10 = string.Empty;
        }
    }
    #endregion
    
     #region TsapReceiptUploadJobNo
    //
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TsapReceiptUploadJobNo
    {
        /// <summary>
        /// 入库手动上传数据主键
        /// </summary>
        public String Sysid { get; set; }
        /// <summary>
        /// 上传时间
        /// </summary>
        public String CreatedOn { get; set; }
        /// <summary>
        /// 上传操作工
        /// </summary>
        public String CreatedBy { get; set; }
        /// <summary>
        /// 出货柜号
        /// </summary>
        public String JobNo { get; set; }
        /// <summary>
        /// 自定义柜号
        /// </summary>
        public String InnerJobNo { get; set; }
        /// <summary>
        /// 标记同一次上传的数据的Key
        /// </summary>
        public String GroupHistKey { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv01 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv02 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv03 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv04 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv05 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv06 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv07 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv08 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv09 { get; set; }
        /// <summary>
        /// 预留栏位
        /// </summary>
        public String Resv10 { get; set; }

        //构造函数
        /// <summary>
        /// 构造函数
        /// </summary>
        public TsapReceiptUploadJobNo()
        {
            Sysid = string.Empty;
            CreatedOn = string.Empty;
            CreatedBy = string.Empty;
            JobNo = string.Empty;
            InnerJobNo = string.Empty;
            GroupHistKey = string.Empty;
            Resv01 = string.Empty;
            Resv02 = string.Empty;
            Resv03 = string.Empty;
            Resv04 = string.Empty;
            Resv05 = string.Empty;
            Resv06 = string.Empty;
            Resv07 = string.Empty;
            Resv08 = string.Empty;
            Resv09 = string.Empty;
            Resv10 = string.Empty;
        }
    }
    #endregion
    
    
     public class MODULEPACKINGTRANSACTION
    {
        /// <summary>
        /// 主键
        /// </summary>
        public String SysID { get; set; }
        /// <summary>
        /// 组件号码
        /// </summary>
        public String ModuleSn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Barcode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String WorkOrder { get; set; }
        /// <summary>
        /// 版型/产品型号
        /// </summary>
        public String ModuleType { get; set; }
        /// <summary>
        /// 产品编号（MES）
        /// </summary>
        public String ProductNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Workshop { get; set; }
        /// <summary>
        /// 交易ID，用以关联T_Carton_Packing_Transaction
        /// </summary>
        public String CartonNoSysID { get; set; }
        /// <summary>
        /// 实测功率
        /// </summary>
        public String Pmax { get; set; }
        /// <summary>
        /// 标称功率
        /// </summary>
        public String ModuleStdPower { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String Temp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String ISC { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String VOC { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String IMP { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String VMP { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String FF { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String EFF { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public String TestDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public String CreatedOn { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public String CreatedBy { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public String ModifiedOn { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public String ModifiedBy { get; set; }
        /// <summary>
        /// 预留字段1
        /// </summary>
        public String Resv01 { get; set; }
        /// <summary>
        /// 预留字段2
        /// </summary>
        public String Resv02 { get; set; }
        /// <summary>
        /// 预留字段3
        /// </summary>
        public String Resv03 { get; set; }
        /// <summary>
        /// 预留字段4
        /// </summary>
        public String Resv04 { get; set; }
        /// <summary>
        /// 预留字段5
        /// </summary>
        public String Resv05 { get; set; }
        /// <summary>
        /// 预留字段6
        /// </summary>
        public String Resv06 { get; set; }
        /// <summary>
        /// 预留字段7
        /// </summary>
        public String Resv07 { get; set; }
        /// <summary>
        /// 预留字段8
        /// </summary>
        public String Resv08 { get; set; }
        /// <summary>
        /// 预留字段9
        /// </summary>
        public String Resv09 { get; set; }
        /// <summary>
        /// 预留字段10
        /// </summary>
        public String Resv10 { get; set; }

    }
    
      public class CARTONPACKINGTRANSACTION
        {
            /// <summary>
            /// 主键
            /// </summary>
            public String SysID { get; set; }
            /// <summary>
            /// 阿特斯组织机构（包括‘CS’，‘LY’，‘OutSea’,'OutSourcing'）
            /// </summary>
            public String Orgnization { get; set; }
            /// <summary>
            /// 箱号
            /// </summary>
            public String Carton { get; set; }
            /// <summary>
            /// 柜号
            /// </summary>
            public String JobNo { get; set; }
            /// <summary>
            /// 操作组件相关的动作（包括：Inventory,UnInventory,LCL,unLCL,OutSeaLCL,OutSeaUnLCL,OutSourcingLCL,OutSourcingUnLCL）
            /// </summary>
            public String Action { get; set; }
            /// <summary>
            /// 交易ID，用以关联T_Module_Packing_Transaction
            /// </summary>
            public String ActionTxnID { get; set; }
            /// <summary>
            /// 操作组件相关的动作的时间
            /// </summary>
            public String ActionDate { get; set; }
            /// <summary>
            /// 操作人
            /// </summary>
            public String ActionBy { get; set; }
            /// <summary>
            /// 原始托号
            /// </summary>
            public String OriginalCarton { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public String OriginalJobNo { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public String StdPowerLevel { get; set; }
            /// <summary>
            /// 组件数量
            /// </summary>
            public String ModuleQty { get; set; }
            /// <summary>
            /// 创建时间
            /// </summary>
            public String CreatedOn { get; set; }
            /// <summary>
            /// 创建人
            /// </summary>
            public String CreatedBy { get; set; }
            /// <summary>
            /// 修改日期
            /// </summary>
            public String ModifiedOn { get; set; }
            /// <summary>
            /// 修改人
            /// </summary>
            public String ModifiedBy { get; set; }
            /// <summary>
            /// 预留字段1
            /// </summary>
            public String Resv01 { get; set; }
            /// <summary>
            /// 预留字段2
            /// </summary>
            public String Resv02 { get; set; }
            /// <summary>
            /// 预留字段3
            /// </summary>
            public String Resv03 { get; set; }
            /// <summary>
            /// 预留字段4
            /// </summary>
            public String Resv04 { get; set; }
            /// <summary>
            /// 预留字段5
            /// </summary>
            public String Resv05 { get; set; }
            /// <summary>
            /// 预留字段6
            /// </summary>
            public String Resv06 { get; set; }
            /// <summary>
            /// 预留字段7
            /// </summary>
            public String Resv07 { get; set; }
            /// <summary>
            /// 预留字段8
            /// </summary>
            public String Resv08 { get; set; }
            /// <summary>
            /// 预留字段9
            /// </summary>
            public String Resv09 { get; set; }
            /// <summary>
            /// 预留字段10
            /// </summary>
            public String Resv10 { get; set; }

        }
      
       public partial class CartonPackingTransactionDal
     {
         public CartonPackingTransactionDal()
         { }
         /// <summary>
         /// 保存数据
         /// </summary>

         public bool SaveStorage(SqlConnection sql1, SqlTransaction trans,List<CARTONPACKINGTRANSACTION> _ListCartonpPacking, List<MODULEPACKINGTRANSACTION> _ListModulePacking, out string msg)
         {
             //try
             //{
                 msg = "";
                 //using (SqlConnection CenterSqlConnection = new SqlConnection(FormCover.CenterDBConnString))
                 //{
                     //CenterSqlConnection.Open();
                     //using (SqlTransaction CenterSqlTransaction = CenterSqlConnection.BeginTransaction())
                     //{
                         try
                         {
                             #region 集中数据库入库记录
                             foreach (MODULEPACKINGTRANSACTION MODULESN in _ListModulePacking)
                             {
                                 if (new MODULEPACKINGTRANSACTIONDAL().Add(sql1, trans, MODULESN, out msg) < 1)
                                 {
                                     msg = "组件 " + MODULESN.ModuleSn + " 在更新集中数据库失败(T_Module_Packing_Transaction)";
                                     trans.Rollback();
                                     break;
                                 }
                             }
                             if (!msg.Equals(""))
                             {
                                 trans.Rollback();
                                 return false;
                             }
                             foreach (CARTONPACKINGTRANSACTION CartonPacking in _ListCartonpPacking)
                             {
                                 if (new CARTONPACKINGTRANSACTIONDAL().Add(sql1, trans, CartonPacking, out msg) < 1)
                                 {
                                     msg = "更新集中数据库失败(T_MODULE_PACKING_TRANSACTION)";
                                     trans.Rollback();
                                     return false;
                                 }
                             }

                             #endregion

                             //CenterSqlTransaction.Commit();
                         }
                         catch (Exception ex)
                         {
                             msg = ex.Message;
                             trans.Rollback();
                             return false;
                         }
                     //}
                 //}
                 return true;
             //}
             //catch (Exception ex)
             //{
             //    msg = ex.Message;
             //    return false;
             //}
         }
     }

       public class MODULEPACKINGTRANSACTIONDAL
    {
        public MODULEPACKINGTRANSACTIONDAL()
        { }
        #region  Method
        /// <summary>
        /// 增加一条数据
        /// </summary>
       
        public int Add(SqlConnection SqlConn, SqlTransaction SqlTrans, MODULEPACKINGTRANSACTION model,out string msg)
        {
            try
            {
                msg = "";
                StringBuilder strSql = new StringBuilder();
                strSql.Append("insert into [CentralizedDatabase].[dbo].[T_Module_Packing_Transaction](");
                strSql.Append("SysID,MODULE_SN,BARCODE,WORK_ORDER,MODULE_TYPE,PRODUCT_NO,WORKSHOP,CartonNoSysID,PMAX,ModuleStdPower,TEMP,ISC,VOC,IMP,VMP,FF,EFF,TEST_DATE,CREATED_BY,MODIFIED_ON,MODIFIED_BY,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV10)");
                strSql.Append(" values (");
                strSql.Append("@SysID,@MODULE_SN,@BARCODE,@WORK_ORDER,@MODULE_TYPE,@PRODUCT_NO,@WORKSHOP,@CartonNoSysID,@PMAX,@ModuleStdPower,@TEMP,@ISC,@VOC,@IMP,@VMP,@FF,@EFF,@TEST_DATE,@CREATED_BY,@MODIFIED_ON,@MODIFIED_BY,@RESV01,@RESV02,@RESV03,@RESV04,@RESV05,@RESV06,@RESV07,@RESV08,@RESV09,@RESV10)");
                SqlParameter[] parameters = {
					new SqlParameter("@SysID", SqlDbType.NVarChar,50),
					new SqlParameter("@MODULE_SN", SqlDbType.NVarChar,50),
                    new SqlParameter("@BARCODE", SqlDbType.NVarChar,50),
                    new SqlParameter("@WORK_ORDER", SqlDbType.NVarChar,50),
                    new SqlParameter("@MODULE_TYPE", SqlDbType.NVarChar,50),
                    new SqlParameter("@PRODUCT_NO", SqlDbType.NVarChar,50),
                    new SqlParameter("@WORKSHOP", SqlDbType.NVarChar,50),
					new SqlParameter("@CartonNoSysID", SqlDbType.NVarChar,50),
                    new SqlParameter("@PMAX", SqlDbType.NVarChar,50),
					new SqlParameter("@ModuleStdPower", SqlDbType.NVarChar,50),
					new SqlParameter("@TEMP", SqlDbType.NVarChar,50),
                    new SqlParameter("@ISC", SqlDbType.NVarChar,50),
                    new SqlParameter("@VOC", SqlDbType.NVarChar,50),
                    new SqlParameter("@IMP", SqlDbType.NVarChar,50),
                    new SqlParameter("@VMP", SqlDbType.NVarChar,50),
                    new SqlParameter("@FF", SqlDbType.NVarChar,50),
                    new SqlParameter("@EFF", SqlDbType.NVarChar,50),
                    new SqlParameter("@TEST_DATE", SqlDbType.NVarChar,50),                    
					//new SqlParameter("@CREATED_ON", SqlDbType.NVarChar,50),
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,100),
					new SqlParameter("@MODIFIED_ON", SqlDbType.NVarChar,100),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV01", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV03", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV06", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV07", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV08", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV09", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV10", SqlDbType.NVarChar,100)
                                        };
                parameters[0].Value = model.SysID;
                parameters[1].Value = model.ModuleSn;
                parameters[2].Value = model.Barcode;
                parameters[3].Value = model.WorkOrder;
                parameters[4].Value = model.ModuleType;
                parameters[5].Value = model.ProductNo;
                parameters[6].Value = model.Workshop;
                parameters[7].Value = model.CartonNoSysID;
                parameters[8].Value = model.Pmax;
                parameters[9].Value = model.ModuleStdPower;
                parameters[10].Value = model.Temp;
                parameters[11].Value = model.ISC;
                parameters[12].Value = model.VOC;
                parameters[13].Value = model.IMP;
                parameters[14].Value = model.VMP;
                parameters[15].Value = model.FF;
                parameters[16].Value = model.EFF;
                parameters[17].Value = model.TestDate;
               // parameters[18].Value = model.CreatedOn;
                parameters[18].Value = model.CreatedBy;
                parameters[19].Value = model.ModifiedOn;
                parameters[20].Value = model.ModifiedBy;
                parameters[21].Value = model.Resv01;
                parameters[22].Value = model.Resv02;
                parameters[23].Value = model.Resv03;
                parameters[24].Value = model.Resv04;
                parameters[25].Value = model.Resv05;
                parameters[26].Value = model.Resv06;
                parameters[27].Value = model.Resv07;
                parameters[28].Value = model.Resv08;
                parameters[29].Value = model.Resv09;
                parameters[30].Value = model.Resv10;
                string _strSql = string.Format(strSql.ToString(), SapUploadServices.CenterConn.Trim());
                return SqlHelper.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, _strSql, parameters);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                return 0;
            }

        #endregion
        }
    }
       
      public class CARTONPACKINGTRANSACTIONDAL
        {
         public CARTONPACKINGTRANSACTIONDAL()
            { }
            #region  Method
            /// <summary>
            /// 增加一条数据
            /// </summary>
            public int Add(SqlConnection SqlConn, SqlTransaction SqlTrans, CARTONPACKINGTRANSACTION model,out string msg)
            {
                try
                {
                    msg = "";
                    StringBuilder strSql = new StringBuilder();
                    strSql.Append("insert into [CentralizedDatabase].[dbo].[T_Carton_Packing_Transaction](");
                    strSql.Append("SysID,Orgnization,Carton,JobNo,Action,ActionTxnID,ActionDate,ActionBy,OriginalCarton,OriginalJobNo,StdPowerLevel,ModuleQty,CREATED_BY,MODIFIED_ON,MODIFIED_BY,RESV01,RESV02,RESV03,RESV04,RESV05,RESV06,RESV07,RESV08,RESV09,RESV10)");
                    strSql.Append(" values (");
                    strSql.Append("@SYSID,@Orgnization,@Carton,@JobNo,@Action,@ActionTxnID,@ActionDate,@ActionBy,@OriginalCarton,@OriginalJobNo,@StdPowerLevel,@ModuleQty,@CREATED_BY,@MODIFIED_ON,@MODIFIED_BY,@RESV01,@RESV02,@RESV03,@RESV04,@RESV05,@RESV06,@RESV07,@RESV08,@RESV09,@RESV10)");
                    SqlParameter[] parameters = {
					new SqlParameter("@SYSID", SqlDbType.NVarChar,50),
					new SqlParameter("@Orgnization", SqlDbType.NVarChar,50),
					new SqlParameter("@Carton", SqlDbType.NVarChar,50),
					new SqlParameter("@JobNo", SqlDbType.NVarChar,50),
					new SqlParameter("@Action", SqlDbType.NVarChar,50),
					new SqlParameter("@ActionTxnID", SqlDbType.NVarChar,50),
					new SqlParameter("@ActionDate", SqlDbType.NVarChar,50),
					new SqlParameter("@ActionBy", SqlDbType.NVarChar,50),
	                new SqlParameter("@OriginalCarton", SqlDbType.NVarChar,50),	
                    new SqlParameter("@OriginalJobNo", SqlDbType.NVarChar,50),	
                    new SqlParameter("@StdPowerLevel", SqlDbType.NVarChar,50),	
                    new SqlParameter("@ModuleQty", SqlDbType.NVarChar,50),	
                   // new SqlParameter("@CREATED_ON", SqlDbType.NVarChar,100),			
					new SqlParameter("@CREATED_BY", SqlDbType.NVarChar,100),
					new SqlParameter("@MODIFIED_ON", SqlDbType.NVarChar,100),
					new SqlParameter("@MODIFIED_BY", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV01", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV02", SqlDbType.NVarChar,100),
                    new SqlParameter("@RESV03", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV04", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV05", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV06", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV07", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV08", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV09", SqlDbType.NVarChar,100),
					new SqlParameter("@RESV10", SqlDbType.NVarChar,100)};
                    parameters[0].Value = model.SysID;
                    parameters[1].Value = model.Orgnization;
                    parameters[2].Value = model.Carton;
                    parameters[3].Value = model.JobNo;
                    parameters[4].Value = model.Action;
                    parameters[5].Value = model.ActionTxnID;
                    parameters[6].Value = model.ActionDate;
                    parameters[7].Value = model.ActionBy;
                    parameters[8].Value = model.OriginalCarton;
                    parameters[9].Value = model.OriginalJobNo;
                    parameters[10].Value = model.StdPowerLevel;
                    parameters[11].Value = model.ModuleQty;
                   // parameters[12].Value = model.CreatedOn;
                    parameters[12].Value = model.CreatedBy;
                    parameters[13].Value = model.ModifiedOn;
                    parameters[14].Value = model.ModifiedBy;
                    parameters[15].Value = model.Resv01;
                    parameters[16].Value = model.Resv02;
                    parameters[17].Value = model.Resv03;
                    parameters[18].Value = model.Resv04;
                    parameters[19].Value = model.Resv05;
                    parameters[20].Value = model.Resv06;
                    parameters[21].Value = model.Resv07;
                    parameters[22].Value = model.Resv08;
                    parameters[23].Value = model.Resv09;
                    parameters[24].Value = model.Resv10;
                    //return DbHelperSQL.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, strSql, parameters);
                    // return  DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
                    string _strSql = string.Format(strSql.ToString(), SapUploadServices.CenterConn.Trim());
                    return SqlHelper.ExecuteNonQuery(SqlConn, SqlTrans, CommandType.Text, _strSql, parameters);
                }
                catch (Exception ex)
                {
                    msg = ex.Message;
                    return 0;
                }
                //DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            }
            #endregion

        }

 public static class SqlHelper
    {
        ///// <summary>
        ///// 从config.xml 获取集中数据库连接字符串
        ///// </summary>
        //private static string _ConnectString;

        ///// <summary>
        ///// 从config.xml 获取集中数据库连接字符串
        ///// </summary>
        //public static string ConnectString
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_ConnectString))
        //            _ConnectString = ToolsClass.getConfig("CenterDB", false, "", "config.xml");
        //        return _ConnectString;
        //    }
        //}

        //private static string _SapConnectString;
        ///// <summary>
        ///// 连接sap数据库
        ///// </summary>
        //public static string SapConnectString
        //{
        //    get
        //    {
        //        if (string.IsNullOrEmpty(_SapConnectString))
        //            _SapConnectString = ToolsClass.getConfig("SapDB", false, "", "config.xml");
        //        return _SapConnectString;
        //    }
        //}

        #region 私有方法

        /// <summary>
        /// 将参数数组附加到SqlCommand上
        /// 
        /// 如果参数是InputOutput的并且value为null的，就自动设置value为DBNull
        /// </summary>
        /// <param name="command">要被附加参数的命令</param>
        /// <param name="commandParameters">参数数组</param>
        private static void AttachParameters(SqlCommand command, IEnumerable<SqlParameter> commandParameters)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            if (commandParameters == null)
                return;

            foreach (var param in commandParameters)
            {
                if (param == null)
                    continue;

                // 检查参数输入输出方向并为空值设置DBNull
                if ((param.Direction == ParameterDirection.InputOutput ||
                     param.Direction == ParameterDirection.Input) &&
                    (param.Value == null))
                {
                    param.Value = DBNull.Value;
                }
                command.Parameters.Add(param);
            }
        }

        /// <summary>
        /// 给命令附加连接，事务，参数等
        /// </summary>
        /// <param name="command">命令</param>
        /// <param name="connection">执行命令使用的连接</param>
        /// <param name="transaction">执行命令的事务，可以为null</param>
        /// <param name="commandType">命令的类型（存储过程或SQL）</param>
        /// <param name="commandText">命令的内容</param>
        /// <param name="commandParameters">参数数组，可以为null</param>
        /// <param name="mustCloseConnection">当连接时有本方法打开时为<c>true</c>，否则为false</param>
        private static void PrepareCommand(SqlCommand command, SqlConnection connection, SqlTransaction transaction,
            CommandType commandType, string commandText, IEnumerable<SqlParameter> commandParameters,
            out bool mustCloseConnection)
        {
            if (command == null)
                throw new ArgumentNullException("command");

            if (string.IsNullOrEmpty(commandText))
                throw new ArgumentNullException("commandText");

            // 如果提供的连接没有打开，则打开它
            if (connection.State != ConnectionState.Open)
            {
                mustCloseConnection = true;
                connection.Open();
            }
            else
            {
                mustCloseConnection = false;
            }
            // 将连接附加到命令上
            command.Connection = connection;
            // 设置命令的内容
            command.CommandText = commandText;
            // 如果需要是用事务，给命令设置事务
            if (transaction != null)
            {
                if (transaction.Connection == null)
                    throw new ArgumentException("事务被回滚或关闭，请提供一个有效的事务", "transaction");
                command.Transaction = transaction;
            }
            // 设置命令类型
            command.CommandType = commandType;
            // 设置命令参数
            if (commandParameters != null)
            {
                AttachParameters(command, commandParameters);
            }
            return;
        }

        #endregion

        #region ExecuteNonQuery

        /// <summary>
        /// 执行数据库操作命令（无参数无返回值）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders");
        /// </remarks>
        /// <param name="connectionString">有效的数据库连接字符串</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <returns>命令影响的行数</returns>
        public static int ExecuteNonQuery(string connectionString, CommandType commandType, string commandText)
        {
            return ExecuteNonQuery(connectionString, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（无返回值）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">有效的数据库连接字符串</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令的参数</param>
        /// <returns>命令影响的行数</returns>
        public static int ExecuteNonQuery(string connectionString, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString");

            // 创建并打开一个连接，使用完后自动关闭
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                return ExecuteNonQuery(connection, commandType, commandText, commandParameters);
            }
        }

        /// <summary>
        /// 执行数据库操作命令（无参数无返回值）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int result = ExecuteNonQuery(conn, CommandType.StoredProcedure, "PublishOrders");
        /// </remarks>
        /// <param name="connection">有效的数据库连接</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <returns>命令影响的行数</returns>
        public static int ExecuteNonQuery(SqlConnection connection, CommandType commandType, string commandText)
        {
            return ExecuteNonQuery(connection, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（无返回值）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int result = ExecuteNonQuery(conn, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connection">有效的数据库连接</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令的参数</param>
        /// <returns>命令影响的行数</returns>
        public static int ExecuteNonQuery(SqlConnection connection, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            return ExecuteNonQuery(connection, null, commandType, commandText, commandParameters);
        }

        /// <summary>
        /// 执行数据库操作命令（无返回值）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int result = ExecuteNonQuery(conn, trans, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connection">有效的数据库连接</param>
        /// <param name="transaction">有效的数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令的参数</param>
        /// <returns>命令影响的行数</returns>
        public static int ExecuteNonQuery(SqlConnection connection, SqlTransaction transaction, CommandType commandType,
            string commandText, params SqlParameter[] commandParameters)
        {
            if (connection == null) throw new ArgumentNullException("connection");

            // 创建一个命令并配置好
            var cmd = new SqlCommand();
            bool mustCloseConnection;
            PrepareCommand(cmd, connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);

            // 执行命令
            var retval = cmd.ExecuteNonQuery();

            // 释放
            cmd.Parameters.Clear();
            if (mustCloseConnection)
                connection.Close();
            return retval;
        }

        /// <summary>
        /// 执行数据库操作命令（无参数无返回值）带事务
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int result = ExecuteNonQuery(trans, CommandType.StoredProcedure, "PublishOrders");
        /// </remarks>
        /// <param name="transaction">有效的数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <returns>命令影响的行数</returns>
        public static int ExecuteNonQuery(SqlTransaction transaction, CommandType commandType, string commandText)
        {
            return ExecuteNonQuery(transaction, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（无返回值）带事务
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int result = ExecuteNonQuery(trans, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="transaction">有效的数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>命令影响的行数</returns>
        public static int ExecuteNonQuery(SqlTransaction transaction, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            if (transaction == null) throw new ArgumentNullException("transaction");
            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("事务被回滚或关闭，请提供一个有效的事务", "transaction");

            // 创建一个命令并配置好
            var cmd = new SqlCommand();
            bool mustCloseConnection;
            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);

            // 执行命令
            var retval = cmd.ExecuteNonQuery();

            // 释放
            cmd.Parameters.Clear();
            return retval;
        }

        #endregion ExecuteNonQuery

        #region ExecuteDataset

        /// <summary>
        /// 执行数据库操作命令（无参数返回结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  DataSet ds = ExecuteDataset(connString, CommandType.StoredProcedure, "GetOrders");
        /// </remarks>
        /// <param name="connectionString">有效的数据库连接字符串</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <returns>结果集</returns>
        public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText)
        {
            return ExecuteDataset(connectionString, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（返回结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  DataSet ds = ExecuteDataset(connString, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">有效的数据库连接字符串</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>结果集</returns>
        public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            
        	
        	if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString");

            // 创建并打开一个连接，使用完后自动关闭
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                return ExecuteDataset(connection, commandType, commandText, commandParameters);
            }
        }

        /// <summary>
        /// 执行数据库操作命令（无参数返回结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  DataSet ds = ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrders");
        /// </remarks>
        /// <param name="connection">有效的数据库连接</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <returns>结果集</returns>
        public static DataSet ExecuteDataset(SqlConnection connection, CommandType commandType, string commandText)
        {
            return ExecuteDataset(connection, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（返回结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  DataSet ds = ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connection">有效的数据库连接</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>结果集</returns>
        public static DataSet ExecuteDataset(SqlConnection connection, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            return ExecuteDataset(connection, null, commandType, commandText, commandParameters);
        }

        /// <summary>
        /// 执行数据库操作命令（返回结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  DataSet ds = ExecuteDataset(conn, trans, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connection">有效的数据库连接</param>
        /// <param name="transaction">有效的数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>结果集</returns>
        public static DataSet ExecuteDataset(SqlConnection connection, SqlTransaction transaction, CommandType commandType,
            string commandText, params SqlParameter[] commandParameters)
        {
            if (connection == null)
                throw new ArgumentNullException("connection");

            // 创建一个命令并准备好
            var cmd = new SqlCommand();
            bool mustCloseConnection;
            PrepareCommand(cmd, connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);

            // 创建DataAdapter和DataSet
            using (var da = new SqlDataAdapter(cmd))
            {
                var ds = new DataSet();
                da.Fill(ds);
                cmd.Parameters.Clear();

                if (mustCloseConnection)
                    connection.Close();

                
                return ds;
            }
        }

        /// <summary>
        /// 执行数据库操作命令（返回结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  DataSet ds = ExecuteDataset(trans, CommandType.StoredProcedure, "GetOrders");
        /// </remarks>
        /// <param name="transaction">有效的数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <returns>结果集</returns>
        public static DataSet ExecuteDataset(SqlTransaction transaction, CommandType commandType, string commandText)
        {
            return ExecuteDataset(transaction, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（返回结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  DataSet ds = ExecuteDataset(trans, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="transaction">有效的数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>结果集</returns>
        public static DataSet ExecuteDataset(SqlTransaction transaction, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException("transaction");

            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("事务被回滚或关闭，请提供一个有效的事务", "transaction");

            // 创建命令
            var cmd = new SqlCommand();
            bool mustCloseConnection;
            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);

            using (var da = new SqlDataAdapter(cmd))
            {
                var ds = new DataSet();
                da.Fill(ds);
                cmd.Parameters.Clear();

                return ds;
            }
        }

        #endregion ExecuteDataset

        #region ExecuteReader

        /// <summary>
        /// 用来说明连接是由调用者创建还是有SqlHelper创建，从而在调用ExecuteReader()的时候确定适当的CommandBehavior
        /// </summary>
        private enum SqlConnectionOwnership
        {
            /// <summary>
            /// 连接属于SqlHelper
            /// </summary>
            Internal,
            /// <summary>
            /// 连接属于调用者
            /// </summary>
            External
        }

        /// <summary>
        /// 执行数据库操作命令（返回Reader）
        /// </summary>
        /// <param name="connection">数据库连接</param>
        /// <param name="transaction">数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令内容</param>
        /// <param name="connectionOwnership">连接属性</param>
        /// <returns>执行结果</returns>
        private static SqlDataReader ExecuteReader(SqlConnection connection, SqlTransaction transaction,
            CommandType commandType, string commandText, IEnumerable<SqlParameter> commandParameters,
            SqlConnectionOwnership connectionOwnership)
        {
            if (connection == null) throw new ArgumentNullException("connection");

            var mustCloseConnection = false;
            var cmd = new SqlCommand();
            try
            {
                PrepareCommand(cmd, connection, transaction, commandType, commandText, commandParameters, out mustCloseConnection);
                var dataReader = connectionOwnership ==
                                   SqlConnectionOwnership.External ? cmd.ExecuteReader() :
                                                                     cmd.ExecuteReader(CommandBehavior.CloseConnection);
                var canClear = true;
                foreach (SqlParameter commandParameter in cmd.Parameters)
                {
                    if (commandParameter.Direction != ParameterDirection.Input)
                        canClear = false;
                }
                if (canClear)
                {
                    cmd.Parameters.Clear();
                }
                return dataReader;
            }
            catch
            {
                if (mustCloseConnection)
                    connection.Close();
                throw;
            }
        }

        /// <summary>
        /// 执行数据库操作命令（返回Reader） 
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  SqlDataReader dr = ExecuteReader(connString, CommandType.StoredProcedure, "GetOrders");
        /// </remarks>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <returns>返回SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(string connectionString, CommandType commandType, string commandText)
        {
            return ExecuteReader(connectionString, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（返回Reader） 
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  SqlDataReader dr = ExecuteReader(connString, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>返回SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(string connectionString, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString");

            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
                return ExecuteReader(connection, null, commandType, commandText, commandParameters,
                    SqlConnectionOwnership.Internal);
            }
            catch
            {
                if (connection != null) connection.Close();
                throw;
            }
        }

        /// <summary>
        /// 执行数据库操作命令（返回Reader）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  SqlDataReader dr = ExecuteReader(conn, CommandType.StoredProcedure, "GetOrders");
        /// </remarks>
        /// <param name="connection">数据库连接</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令参数</param>
        /// <returns>返回SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(SqlConnection connection, CommandType commandType, string commandText)
        {
            return ExecuteReader(connection, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（返回Reader）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  SqlDataReader dr = ExecuteReader(conn, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connection">数据库连接</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令参数</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>返回SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(SqlConnection connection, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            return ExecuteReader(connection, null, commandType, commandText, commandParameters,
                SqlConnectionOwnership.External);
        }

        /// <summary>
        /// 执行数据库操作命令（返回Reader）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  SqlDataReader dr = ExecuteReader(trans, CommandType.StoredProcedure, "GetOrders");
        /// </remarks>
        /// <param name="transaction">数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令参数</param>
        /// <returns>返回SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(SqlTransaction transaction, CommandType commandType, string commandText)
        {
            return ExecuteReader(transaction, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（返回Reader）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///   SqlDataReader dr = ExecuteReader(trans, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="transaction">数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令参数</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>返回SqlDataReader</returns>
        public static SqlDataReader ExecuteReader(SqlTransaction transaction, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException("transaction");

            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("事务被回滚或关闭，请提供一个有效的事务", "transaction");

            return ExecuteReader(transaction.Connection, transaction, commandType, commandText, commandParameters,
                SqlConnectionOwnership.External);
        }

        #endregion ExecuteReader

        #region ExecuteScalar

        /// <summary>
        /// 执行数据库操作命令（返回1*1结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int orderCount = (int)ExecuteScalar(connString, CommandType.StoredProcedure, "GetOrderCount");
        /// </remarks>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <returns>返回1*1结果集</returns>
        public static object ExecuteScalar(string connectionString, CommandType commandType, string commandText)
        {
            return ExecuteScalar(connectionString, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（返回1*1结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int orderCount = (int)ExecuteScalar(connString, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>返回1*1结果集</returns>
        public static object ExecuteScalar(string connectionString, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentNullException("connectionString");

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();

                return ExecuteScalar(connection, commandType, commandText, commandParameters);
            }
        }

        /// <summary>
        /// 执行数据库操作命令（返回1*1结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int orderCount = (int)ExecuteScalar(conn, CommandType.StoredProcedure, "GetOrderCount");
        /// </remarks>
        /// <param name="connection">数据库连接</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <returns>返回1*1结果集</returns>
        public static object ExecuteScalar(SqlConnection connection, CommandType commandType, string commandText)
        {
            return ExecuteScalar(connection, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（返回1*1结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int orderCount = (int)ExecuteScalar(conn, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connection">数据库连接</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>返回1*1结果集</returns>
        public static object ExecuteScalar(SqlConnection connection, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            return ExecuteScalar(connection, null, commandType, commandText, commandParameters);
        }

        /// <summary>
        /// 执行数据库操作命令（返回1*1结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int orderCount = (int)ExecuteScalar(conn, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connection">数据库连接</param>
        /// <param name="transaction">数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>返回1*1结果集</returns>
        public static object ExecuteScalar(SqlConnection connection, SqlTransaction transaction, CommandType commandType,
            string commandText, params SqlParameter[] commandParameters)
        {
            if (connection == null) throw new ArgumentNullException("connection");

            var cmd = new SqlCommand();
            bool mustCloseConnection;
            PrepareCommand(cmd, connection, transaction, commandType, commandText, commandParameters,
                out mustCloseConnection);
            var retval = cmd.ExecuteScalar();
            cmd.Parameters.Clear();

            if (mustCloseConnection)
                connection.Close();
            return retval;
        }

        /// <summary>
        /// 执行数据库操作命令（返回1*1结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int orderCount = (int)ExecuteScalar(trans, CommandType.StoredProcedure, "GetOrderCount");
        /// </remarks>
        /// <param name="transaction">数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <returns>返回1*1结果集</returns>
        public static object ExecuteScalar(SqlTransaction transaction, CommandType commandType, string commandText)
        {
            return ExecuteScalar(transaction, commandType, commandText, null);
        }

        /// <summary>
        /// 执行数据库操作命令（返回1*1结果集）
        /// </summary>
        /// <remarks>
        /// 示例：
        ///  int orderCount = (int)ExecuteScalar(trans, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="transaction">数据库事务</param>
        /// <param name="commandType">命令类型</param>
        /// <param name="commandText">命令内容</param>
        /// <param name="commandParameters">命令参数</param>
        /// <returns>返回1*1结果集</returns>
        public static object ExecuteScalar(SqlTransaction transaction, CommandType commandType, string commandText,
            params SqlParameter[] commandParameters)
        {
            if (transaction == null)
                throw new ArgumentNullException("transaction");

            if (transaction != null && transaction.Connection == null)
                throw new ArgumentException("事务被回滚或关闭，请提供一个有效的事务", "transaction");

            var cmd = new SqlCommand();
            bool mustCloseConnection;
            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters,
                out mustCloseConnection);
            var retval = cmd.ExecuteScalar();
            cmd.Parameters.Clear();
            return retval;
        }
        #endregion
    }
      
}
